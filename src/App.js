import React from "react";
import "./App.css";
import CharacterList from "./Component/CharacterList";
import HeroList from "./Component/HeroList";
import SquadStats from "./Component/SquadStats";

function App() {
  return (
    <div className="App">
      <div className="col-md-4">
        <CharacterList />
      </div>
      <div className="col-md-4">
        <HeroList />
      </div>
      <div className="col-md-4">
        <SquadStats />
      </div>
    </div>
  );
}

export default App;
