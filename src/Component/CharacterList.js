import React, { Component } from "react";
import { connect } from "react-redux";
import { addCharacterById } from "../Action/index";
class CharacterList extends Component {
  render() {
   
    return (
      <div>
        <h1>CharacterList</h1>
        <ul className="list-group">
          {this.props.characters.map((character, index) => {
            return (
              <li className="list-group-item" key={index}>
                <div className="list-item">{character.name}</div>
                <div
                  className="list-item right-button"
                  onClick={() => this.props.addCharacterById(character.id)}
                >
                  +
                </div>
              </li>
            );
          })}
        </ul>
      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    characters: state.characters
  };
}
export default connect(mapStateToProps, { addCharacterById })(CharacterList);
