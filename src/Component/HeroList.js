import React, { Component } from "react";
import { connect } from "react-redux";
import { removeCharacterById } from "../Action/index";

class HeroList extends Component {
  render() {
    return (
      <div>
        <h1>HeroList</h1>
        <ul className="list-group">
          {this.props.heroes.map((hero, index) => {
            return (
              <li key={index} className="list-group-item">
                <div className="list-item">{hero.name}</div>
                <div
                  className="list-item right-button"
                  onClick={() => this.props.removeCharacterById(hero.id)}
                >
                  X
                </div>
              </li>
            );
          })}
        </ul>
      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    heroes: state.heroes
  };
}
export default connect(mapStateToProps, { removeCharacterById })(HeroList);
