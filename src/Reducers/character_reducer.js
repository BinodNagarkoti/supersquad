import characters_json from "../characters.json";
import { ADD_CHARACTER, REMOVE_CHARACTER } from "../Action/index";
import { createCharacter } from "./helpers";

let characters = [];
function character(state = characters_json, action) {
  switch (action.type) {
    case ADD_CHARACTER:
      characters = state.filter(item => item.id !== action.id);
      return characters;
    case REMOVE_CHARACTER:
      console.log(REMOVE_CHARACTER);
      characters = [...state, createCharacter(action.id)];
      return characters;
    default:
      return state;
  }
}

export default character;
