import characters from "./character_reducer";
import heroes from "./hero_reducer";
import {combineReducers} from "redux";
const allReducers = combineReducers({
  characters,
  heroes
});
export default allReducers;
