import { ADD_CHARACTER, REMOVE_CHARACTER } from "../Action/index";
import { createCharacter } from "./helpers";
let heroes;
function heroe(state = [], action) {
  switch (action.type) {
    case ADD_CHARACTER:
      heroes = [...state, createCharacter(action.id)];
      return heroes;
    case REMOVE_CHARACTER:
      heroes = state.filter(item => item.id !== action.id);
      return heroes;
    default:
      return state;
  }
}
export default heroe;
